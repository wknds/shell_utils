#!/bin/dash

trennlinie=''
trennlinie_titel=''

set_trennlinie() {
	trennlinie="$(printf %$(tput cols)s | tr " " $1)"
}

set_trennlinie_titel() {
	trennlinie_titel="$(printf %$(tput cols)s | tr " " $1)"
}

trennlinie_einfuegen() {
	echo $trennlinie
}

print_title () {
	echo ""
	echo $trennlinie_titel
	echo ""
	echo "    $1"
	echo ""
	echo $trennlinie_titel
	echo ""
}

print_section () {
	echo ""
	#echo $trennlinie
	echo " -> $1"
	echo $trennlinie
}

set_trennlinie "-"
set_trennlinie_titel "#" 
