#!/bin/bash

if check_internet_connectivity
then
	update_upgrade && apt install -y < $1
else
	printf "Bitte einrichten unter /etc/network/interfaces.d/*\n"
	exit 1;
fi
