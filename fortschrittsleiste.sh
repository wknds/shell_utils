#!/bin/dash

visualisiere_fortschritt_mit_leiste () {
	fortschrittsleisten_breite=$1
	fortschritt_prozent=$2

	printf %$(($fortschrittsleisten_breite * ${fortschritt_prozent} / 100))s | tr " " "|"
	printf %$(($fortschrittsleisten_breite * (100 - $fortschritt_prozent) / 100))s | tr " " " "
	printf "] $fortschritt_prozent"
	echo -n "%\r"
}
