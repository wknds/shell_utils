#!/bin/dash

check_internet_connectivity() {
	printf "Überprüfe Internetverbindung. "
	if ping -q -c 1 -W 1 debian.org > /dev/null
	then
		printf "Ok.\n"
		return 0;
	else
		printf "Keine Internetverbindung.\n"
		return 1;
	fi
}
